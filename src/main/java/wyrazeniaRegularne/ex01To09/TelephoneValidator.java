package wyrazeniaRegularne.ex01To09;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TelephoneValidator {

    private final Pattern pattern8 = Pattern.compile("^[0-9]{9}$|" +
            "^[0-9]{3}-[0-9]{3}-[0-9]{3}$|" +
            "^[0-9]{3} [0-9]{3} [0-9]{3}$|" +
            "^[+]{1}[+0-9]{2} [0-9]{9}$|" +
            "^[+]{1}[0-9]{2} [0-9]{3}-[0-9]{3}-[0-9]{3}$|" +
            "^[+]{1}[0-9]{2} [0-9]{3} [0-9]{3} [0-9]{3}$"

    );

    public boolean isTelephoneNumber(String telephone) {
        Matcher matcher = pattern8.matcher(telephone);
        return matcher.matches();
    }
}

