package wyrazeniaRegularne.ex01To09;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Wprowadź ciąg do sprawdzenia: ");
        Scanner scanner = new Scanner(System.in);
        String napis = scanner.nextLine();

        boolean isUppercase = new TextChecker().isUppercase(napis);
        boolean isLowercase = new TextChecker().isLowercase(napis);
        boolean isNumber = new TextChecker().isNumber(napis);
        boolean isThreeDigitNumber = new TextChecker().isThreeDigitNumber(napis);
        boolean is3To5DigitNumber = new TextChecker().is3To5DigitNumber(napis);
        boolean isLowercaseOrUppercaseOrNumber = new TextChecker().isLowercaseOrUppercaseOrNumber(napis);
        boolean isZipCode = new TextChecker().isZipCode(napis);
        boolean isTelephoneNumber = new TelephoneValidator().isTelephoneNumber(napis);

        if (isUppercase) {
            System.out.println("Ciąg " + napis + " zawiera tylko wielkie litery");
        }
        if (isLowercase) {
            System.out.println("Ciąg " + napis + " zawiera tylko małe litery");
        }

        if (isNumber) {
            System.out.println("Ciąg " + napis + " zawiera tylko cyfry");
        }

        if (isThreeDigitNumber) {
            System.out.println("Ciąg " + napis + " zawiera wylacznie 3 cyfry");
        }

        if (is3To5DigitNumber){
            System.out.println("Ciąg " + napis + " zawiera od 3 do 5 cyfr");
        }

        if (isLowercaseOrUppercaseOrNumber){
            System.out.println("Ciąg " + napis + " zawiera male lub duze litery lub cyfry");
        }

        if (isZipCode){
            System.out.println("Ciąg " + napis + " jest kodem pocztowym");
        }

        if (isTelephoneNumber){
            System.out.println("Ciąg " + napis + " jest numerem telefonu");
        }

    }
}
