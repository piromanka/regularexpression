package wyrazeniaRegularne.ex01To09;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextChecker {
    private final Pattern pattern = Pattern.compile("^[A-Z]*$");
    private final Pattern pattern2 = Pattern.compile("^[a-z]*$");
    private final Pattern pattern3 = Pattern.compile("^[0-9]*$");
    private final Pattern pattern4 = Pattern.compile("^[0-9]{3}$");
    private final Pattern pattern5 = Pattern.compile("^[0-9]{3,5}$");
    private final Pattern pattern6 = Pattern.compile("^[0-9A-Za-z]$");
    private final Pattern pattern7 = Pattern.compile("^[0-9]{2}-[0-9]{3}$");

    public boolean isUppercase(String text) {
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    public boolean isLowercase(String text) {
        Matcher matcher = pattern2.matcher(text);
        return matcher.matches();
    }

    public boolean isNumber(String text) {
        Matcher matcher = pattern3.matcher(text);
        return matcher.matches();
    }

    public boolean isThreeDigitNumber(String text){
        Matcher matcher = pattern4.matcher(text);
        return matcher.matches();
    }

    public boolean is3To5DigitNumber(String text){
        Matcher matcher = pattern5.matcher(text);
        return matcher.matches();
    }

    public boolean isLowercaseOrUppercaseOrNumber(String text){
        Matcher matcher = pattern6.matcher(text);
        return matcher.matches();
    }

    public boolean isZipCode(String text) {
        Matcher matcher = pattern7.matcher(text);
        return matcher.matches();
    }

}
