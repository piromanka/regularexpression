1. Napisz aplikację, która sprawdza czy wprowadzony przez użytkownika ciąg znaków zawiera tylko i wyłącznie duże litery.
2. Napisz aplikację, która sprawdza czy wprowadzony przez użytkownika ciąg znaków zawiera tylko i wyłącznie małe litery.
3. Napisz aplikację, która sprawdza czy wprowadzony przez użytkownika ciąg znaków zawiera tylko i wyłącznie cyfry.
4. Napisz aplikację, która sprawdza, czy wprowadzona przez użytkownika liczba jest 3 cyfrowa
5. Napisz aplikację, która sprawdza, czy wprowadzona przez użytkownika liczba składa się z 3-5 cyfr.
6. Napisz aplikację, która sprawdza czy wprowadzony przez użytkownika ciąg znaków zawiera małe litery lub duże litery lub cyfry.
7. Napisz aplikację, która sprawdza czy wprowadzony przez użytkownika kod pocztowy jest poprawny. Załóż, że format wprowadzonego kodu pocztowego jest następujący
dd-dddgdzie d to cyfra z przedziału <0,9>
8. Napisz aplikację sprawdzającą czy wprowadzony przez użytkownika numer telefonu jest poprawny. Przygotuj klasę o nazwie TelephoneValidator posiadającą metodę
public boolean validate(String telephone);
Podany telefon uważa się za poprawny gdy składa się z 9 cyfr. Poprawny numer telefonu to: 505879357, 505 879 357, 505-879-357 Błędny numer telefonu to: 50 58 79 35 7, 5058794, 44505879357, 505 784-322
9. Rozbuduj aplikację z poprzedniego zadania tak aby mechanizm sprawdzający weryfikował też numer kierunkowy kraju. Przykładowo numer telefonu +48505888159 jak i 505888159 jest poprawny. W celu uproszczenia zadania zakładamy, że numer kierunkowy kraju przyjmuje postać